package adapter;

import java.beans.*;

import com.streambase.sb.operator.parameter.*;

/**
* A BeanInfo class controls what properties are exposed, add 
* metadata about properties (such as which properties are optional), and access 
* special types of properties that can't be automatically derived via reflection. 
* If a BeanInfo class is present, only the properties explicitly declared in
* this class will be exposed by StreamBase.
*/
public class FTLRecvBeanInfo extends SBSimpleBeanInfo {

	/*
	* The order of properties below determines the order they are displayed within
	* the StreamBase Studio property view. 
	*/
	public SBPropertyDescriptor[] getPropertyDescriptorsChecked() throws IntrospectionException {
		SBPropertyDescriptor[] p = {
				new SBPropertyDescriptor("realmServer", FTLRecv.class).displayName("realmServer").description(""),
				new SBPropertyDescriptor("appName", FTLRecv.class).displayName("appName").description(""),
				new SBPropertyDescriptor("durableName", FTLRecv.class).displayName("durableName").description(""),
				new SBPropertyDescriptor("subEndpoint", FTLRecv.class).displayName("subEndpoint").description(""),
				new SBPropertyDescriptor("schema0", FTLRecv.class), };
		return p;
	}

}
